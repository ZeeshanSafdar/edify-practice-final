﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace EdifyPracticeFinal.Data.Migrations
{
    public partial class afterMerge : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Applications_Employees_EmployeeId",
                table: "Applications");

            migrationBuilder.DropForeignKey(
                name: "FK_Applications_Teachers_TeacherId",
                table: "Applications");

            migrationBuilder.DropForeignKey(
                name: "FK_DailyDiaries_Teachers_TeacherId",
                table: "DailyDiaries");

            migrationBuilder.DropForeignKey(
                name: "FK_DailyDiaryDetail_SubjectClassRs_SubjectClassRId",
                table: "DailyDiaryDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_DatesheetDetails_SubjectClassRs_SubjectClassRId1",
                table: "DatesheetDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_Datesheets_ClassExamTestR_ClassExamTestRId",
                table: "Datesheets");

            migrationBuilder.DropForeignKey(
                name: "FK_EmpAttendances_Employees_EmployeeId",
                table: "EmpAttendances");

            migrationBuilder.DropForeignKey(
                name: "FK_Employees_Departments_DepartmentId",
                table: "Employees");

            migrationBuilder.DropForeignKey(
                name: "FK_Employees_Designations_DesignationId",
                table: "Employees");

            migrationBuilder.DropForeignKey(
                name: "FK_Employees_Users_UserId",
                table: "Employees");

            migrationBuilder.DropForeignKey(
                name: "FK_LessonPlans_SubjectClassRs_SubjectClassRId",
                table: "LessonPlans");

            migrationBuilder.DropForeignKey(
                name: "FK_Pays_SalarySlips_SalarySlipId",
                table: "Pays");

            migrationBuilder.DropForeignKey(
                name: "FK_SpecialNotes_StudentSectionRs_StudentSectionRId",
                table: "SpecialNotes");

            migrationBuilder.DropForeignKey(
                name: "FK_Students_Fees_FeeId",
                table: "Students");

            migrationBuilder.DropForeignKey(
                name: "FK_Students_Sections_SectionId",
                table: "Students");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectResultDetails_ClassExamTestR_ClassExamTestRId",
                table: "SubjectResultDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectResultDetails_SubjectResults_SubjectResultId",
                table: "SubjectResultDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectResults_ClassExamTestR_ClassExamTestRId",
                table: "SubjectResults");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectResults_Sections_SectionId",
                table: "SubjectResults");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectResults_SubjectClassRs_SubjectClassRId",
                table: "SubjectResults");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectResults_Teachers_TeacherId",
                table: "SubjectResults");

            migrationBuilder.DropForeignKey(
                name: "FK_Syllabi_ClassExamTestR_ClassExamTestRId",
                table: "Syllabi");

            migrationBuilder.DropForeignKey(
                name: "FK_SyllabusDetails_SubjectClassRs_SubjectClassRId",
                table: "SyllabusDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_TimetableDetails_SubjectClassRs_SubjectClassRId",
                table: "TimetableDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_TimetableDetails_Teachers_TeacherId",
                table: "TimetableDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_TimetableDetails_Timetables_TimetableId",
                table: "TimetableDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Branches_BranchId",
                table: "Users");

            migrationBuilder.DropTable(
                name: "ClassExamTestR");

            migrationBuilder.DropTable(
                name: "StudentSectionRs");

            migrationBuilder.DropTable(
                name: "SubjectClassRs");

            migrationBuilder.DropTable(
                name: "SubjectSectionTeacherRs");

            migrationBuilder.DropIndex(
                name: "IX_TimetableDetails_SubjectClassRId",
                table: "TimetableDetails");

            migrationBuilder.DropIndex(
                name: "IX_TimetableDetails_TeacherId",
                table: "TimetableDetails");

            migrationBuilder.DropIndex(
                name: "IX_TimetableDetails_TimetableId",
                table: "TimetableDetails");

            migrationBuilder.DropIndex(
                name: "IX_Syllabi_ClassExamTestRId",
                table: "Syllabi");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SubjectResults",
                table: "SubjectResults");

            migrationBuilder.DropIndex(
                name: "IX_Students_SectionId",
                table: "Students");

            migrationBuilder.DropIndex(
                name: "IX_Employees_UserId",
                table: "Employees");

            migrationBuilder.DropIndex(
                name: "IX_Applications_EmployeeId",
                table: "Applications");

            migrationBuilder.DropColumn(
                name: "Religion",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "SubjectClassRId",
                table: "TimetableDetails");

            migrationBuilder.DropColumn(
                name: "SectionId",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "PhoneNum",
                table: "Schools");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Employees");

            migrationBuilder.DropColumn(
                name: "EmployeeId",
                table: "Applications");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "AcademicYears");

            migrationBuilder.RenameTable(
                name: "SubjectResults",
                newName: "SubjectResult");

            migrationBuilder.RenameColumn(
                name: "RecentInstitute",
                table: "Teachers",
                newName: "RecentInstitue");

            migrationBuilder.RenameColumn(
                name: "SubjectClassRId",
                table: "SyllabusDetails",
                newName: "SubjectId");

            migrationBuilder.RenameIndex(
                name: "IX_SyllabusDetails_SubjectClassRId",
                table: "SyllabusDetails",
                newName: "IX_SyllabusDetails_SubjectId");

            migrationBuilder.RenameColumn(
                name: "SubjectClassRId",
                table: "SubjectResult",
                newName: "SubjectId");

            migrationBuilder.RenameColumn(
                name: "ClassExamTestRId",
                table: "SubjectResult",
                newName: "ExamTestId");

            migrationBuilder.RenameIndex(
                name: "IX_SubjectResults_TeacherId",
                table: "SubjectResult",
                newName: "IX_SubjectResult_TeacherId");

            migrationBuilder.RenameIndex(
                name: "IX_SubjectResults_SubjectClassRId",
                table: "SubjectResult",
                newName: "IX_SubjectResult_SubjectId");

            migrationBuilder.RenameIndex(
                name: "IX_SubjectResults_SectionId",
                table: "SubjectResult",
                newName: "IX_SubjectResult_SectionId");

            migrationBuilder.RenameIndex(
                name: "IX_SubjectResults_ClassExamTestRId",
                table: "SubjectResult",
                newName: "IX_SubjectResult_ExamTestId");

            migrationBuilder.RenameColumn(
                name: "ClassExamTestRId",
                table: "SubjectResultDetails",
                newName: "ExamTestId");

            migrationBuilder.RenameIndex(
                name: "IX_SubjectResultDetails_ClassExamTestRId",
                table: "SubjectResultDetails",
                newName: "IX_SubjectResultDetails_ExamTestId");

            migrationBuilder.RenameColumn(
                name: "StudentSectionRId",
                table: "SpecialNotes",
                newName: "StudentId");

            migrationBuilder.RenameIndex(
                name: "IX_SpecialNotes_StudentSectionRId",
                table: "SpecialNotes",
                newName: "IX_SpecialNotes_StudentId");

            migrationBuilder.RenameColumn(
                name: "SubjectClassRId",
                table: "LessonPlans",
                newName: "SubjectId");

            migrationBuilder.RenameIndex(
                name: "IX_LessonPlans_SubjectClassRId",
                table: "LessonPlans",
                newName: "IX_LessonPlans_SubjectId");

            migrationBuilder.RenameColumn(
                name: "ClassExamTestRId",
                table: "Datesheets",
                newName: "ExamTestId");

            migrationBuilder.RenameIndex(
                name: "IX_Datesheets_ClassExamTestRId",
                table: "Datesheets",
                newName: "IX_Datesheets_ExamTestId");

            migrationBuilder.RenameColumn(
                name: "SubjectClassRId1",
                table: "DatesheetDetails",
                newName: "SubjectId1");

            migrationBuilder.RenameColumn(
                name: "SubjectClassRId",
                table: "DatesheetDetails",
                newName: "SubjectId");

            migrationBuilder.RenameIndex(
                name: "IX_DatesheetDetails_SubjectClassRId1",
                table: "DatesheetDetails",
                newName: "IX_DatesheetDetails_SubjectId1");

            migrationBuilder.RenameColumn(
                name: "SubjectClassRId",
                table: "DailyDiaryDetail",
                newName: "SubjectId");

            migrationBuilder.RenameIndex(
                name: "IX_DailyDiaryDetail_SubjectClassRId",
                table: "DailyDiaryDetail",
                newName: "IX_DailyDiaryDetail_SubjectId");

            migrationBuilder.RenameColumn(
                name: "TeacherId",
                table: "DailyDiaries",
                newName: "StudentId");

            migrationBuilder.RenameIndex(
                name: "IX_DailyDiaries_TeacherId",
                table: "DailyDiaries",
                newName: "IX_DailyDiaries_StudentId");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Users",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<int>(
                name: "Gender",
                table: "Users",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<int>(
                name: "BranchId",
                table: "Users",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "Users",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                table: "Users",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "Users",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "UserType",
                table: "Users",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Timetables",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "TimetableId",
                table: "TimetableDetails",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "TeacherId",
                table: "TimetableDetails",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "StartTime",
                table: "TimetableDetails",
                nullable: false,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<string>(
                name: "EndTime",
                table: "TimetableDetails",
                nullable: false,
                oldClrType: typeof(DateTime));

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "TimetableDetails",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "SubjectId",
                table: "TimetableDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SubjectId1",
                table: "TimetableDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TeacherId1",
                table: "TimetableDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TimetableId1",
                table: "TimetableDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Teachers",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "SectionId",
                table: "Teachers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "SyllabusDetails",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "ClassId",
                table: "Syllabi",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ExamTestId",
                table: "Syllabi",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Syllabi",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Subjects",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "SubjectResult",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "SubjectResultDetails",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<int>(
                name: "FeeId",
                table: "Students",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Students",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "StudentGatePasses",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "StuAttendances",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "StuAttendanceDetails",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "SectionId",
                table: "SpecialNotes",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Sections",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 10);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "SectionIncharges",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "Schools",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                table: "Schools",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "PhoneNo",
                table: "Schools",
                maxLength: 16,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "Schools",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "SalarySlips",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<int>(
                name: "SalarySlipId",
                table: "Pays",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Pays",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "PayrollCategories",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Parents",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "NotificationDetails",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "NewsUpdates",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "LessonPlans",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "LessonPlanDetails",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Fees",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "StudentId",
                table: "Fees",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "FeeCategories",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "ExamTests",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Events",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<int>(
                name: "DesignationId",
                table: "Employees",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "DepartmentId",
                table: "Employees",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "Department",
                table: "Employees",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Designation",
                table: "Employees",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Employees",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<int>(
                name: "EmployeeId",
                table: "EmpAttendances",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "EmpId",
                table: "EmpAttendances",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "EmpAttendances",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Departments",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Datesheets",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<int>(
                name: "ClassId",
                table: "Datesheets",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "DatesheetDetails",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "DailyDiaryDetail",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "DailyDiaries",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Complaints",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Classes",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Branches",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "Branches",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedAt",
                table: "Branches",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Branches",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TotalStudents",
                table: "Branches",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "Branches",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<int>(
                name: "TeacherId",
                table: "Applications",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_SubjectResult",
                table: "SubjectResult",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "ClassExamTest",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClassId = table.Column<int>(nullable: false),
                    ExamTestId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassExamTest", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClassExamTest_Classes_ClassId",
                        column: x => x.ClassId,
                        principalTable: "Classes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClassExamTest_ExamTests_ExamTestId",
                        column: x => x.ExamTestId,
                        principalTable: "ExamTests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ClassSubject",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClassId = table.Column<int>(nullable: false),
                    SubjectId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassSubject", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClassSubject_Classes_ClassId",
                        column: x => x.ClassId,
                        principalTable: "Classes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClassSubject_Subjects_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SectionStudent",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SectionId = table.Column<int>(nullable: false),
                    StudentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SectionStudent", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SectionStudent_Sections_SectionId",
                        column: x => x.SectionId,
                        principalTable: "Sections",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SectionStudent_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "SectionTeacher",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SectionId = table.Column<int>(nullable: false),
                    TeacherId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SectionTeacher", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SectionTeacher_Sections_SectionId",
                        column: x => x.SectionId,
                        principalTable: "Sections",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SectionTeacher_Teachers_TeacherId",
                        column: x => x.TeacherId,
                        principalTable: "Teachers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StudentSubject",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StudentId = table.Column<int>(nullable: false),
                    SubjectId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentSubject", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StudentSubject_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentSubject_Subjects_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubjectTeacher",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SubjectId = table.Column<int>(nullable: false),
                    TeacherId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubjectTeacher", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubjectTeacher_Subjects_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubjectTeacher_Teachers_TeacherId",
                        column: x => x.TeacherId,
                        principalTable: "Teachers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TimetableDetails_SubjectId1",
                table: "TimetableDetails",
                column: "SubjectId1");

            migrationBuilder.CreateIndex(
                name: "IX_TimetableDetails_TeacherId1",
                table: "TimetableDetails",
                column: "TeacherId1");

            migrationBuilder.CreateIndex(
                name: "IX_TimetableDetails_TimetableId1",
                table: "TimetableDetails",
                column: "TimetableId1");

            migrationBuilder.CreateIndex(
                name: "IX_Teachers_SectionId",
                table: "Teachers",
                column: "SectionId");

            migrationBuilder.CreateIndex(
                name: "IX_Syllabi_ClassId",
                table: "Syllabi",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_Syllabi_ExamTestId",
                table: "Syllabi",
                column: "ExamTestId");

            migrationBuilder.CreateIndex(
                name: "IX_SpecialNotes_SectionId",
                table: "SpecialNotes",
                column: "SectionId");

            migrationBuilder.CreateIndex(
                name: "IX_Fees_StudentId",
                table: "Fees",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_Datesheets_ClassId",
                table: "Datesheets",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassExamTest_ClassId",
                table: "ClassExamTest",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassExamTest_ExamTestId",
                table: "ClassExamTest",
                column: "ExamTestId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassSubject_ClassId",
                table: "ClassSubject",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassSubject_SubjectId",
                table: "ClassSubject",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_SectionStudent_SectionId",
                table: "SectionStudent",
                column: "SectionId");

            migrationBuilder.CreateIndex(
                name: "IX_SectionStudent_StudentId",
                table: "SectionStudent",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_SectionTeacher_SectionId",
                table: "SectionTeacher",
                column: "SectionId");

            migrationBuilder.CreateIndex(
                name: "IX_SectionTeacher_TeacherId",
                table: "SectionTeacher",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentSubject_StudentId",
                table: "StudentSubject",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentSubject_SubjectId",
                table: "StudentSubject",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectTeacher_SubjectId",
                table: "SubjectTeacher",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectTeacher_TeacherId",
                table: "SubjectTeacher",
                column: "TeacherId");

            migrationBuilder.AddForeignKey(
                name: "FK_Applications_Teachers_TeacherId",
                table: "Applications",
                column: "TeacherId",
                principalTable: "Teachers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DailyDiaries_Students_StudentId",
                table: "DailyDiaries",
                column: "StudentId",
                principalTable: "Students",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DailyDiaryDetail_Subjects_SubjectId",
                table: "DailyDiaryDetail",
                column: "SubjectId",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DatesheetDetails_Subjects_SubjectId1",
                table: "DatesheetDetails",
                column: "SubjectId1",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Datesheets_Classes_ClassId",
                table: "Datesheets",
                column: "ClassId",
                principalTable: "Classes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Datesheets_ExamTests_ExamTestId",
                table: "Datesheets",
                column: "ExamTestId",
                principalTable: "ExamTests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmpAttendances_Employees_EmployeeId",
                table: "EmpAttendances",
                column: "EmployeeId",
                principalTable: "Employees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_Departments_DepartmentId",
                table: "Employees",
                column: "DepartmentId",
                principalTable: "Departments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_Designations_DesignationId",
                table: "Employees",
                column: "DesignationId",
                principalTable: "Designations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Fees_Students_StudentId",
                table: "Fees",
                column: "StudentId",
                principalTable: "Students",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_LessonPlans_Subjects_SubjectId",
                table: "LessonPlans",
                column: "SubjectId",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Pays_SalarySlips_SalarySlipId",
                table: "Pays",
                column: "SalarySlipId",
                principalTable: "SalarySlips",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SpecialNotes_Sections_SectionId",
                table: "SpecialNotes",
                column: "SectionId",
                principalTable: "Sections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SpecialNotes_Students_StudentId",
                table: "SpecialNotes",
                column: "StudentId",
                principalTable: "Students",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Students_Fees_FeeId",
                table: "Students",
                column: "FeeId",
                principalTable: "Fees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectResult_ExamTests_ExamTestId",
                table: "SubjectResult",
                column: "ExamTestId",
                principalTable: "ExamTests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectResult_Sections_SectionId",
                table: "SubjectResult",
                column: "SectionId",
                principalTable: "Sections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectResult_Subjects_SubjectId",
                table: "SubjectResult",
                column: "SubjectId",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectResult_Teachers_TeacherId",
                table: "SubjectResult",
                column: "TeacherId",
                principalTable: "Teachers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectResultDetails_ExamTests_ExamTestId",
                table: "SubjectResultDetails",
                column: "ExamTestId",
                principalTable: "ExamTests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectResultDetails_SubjectResult_SubjectResultId",
                table: "SubjectResultDetails",
                column: "SubjectResultId",
                principalTable: "SubjectResult",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Syllabi_Classes_ClassId",
                table: "Syllabi",
                column: "ClassId",
                principalTable: "Classes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Syllabi_ExamTests_ExamTestId",
                table: "Syllabi",
                column: "ExamTestId",
                principalTable: "ExamTests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SyllabusDetails_Subjects_SubjectId",
                table: "SyllabusDetails",
                column: "SubjectId",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Teachers_Sections_SectionId",
                table: "Teachers",
                column: "SectionId",
                principalTable: "Sections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TimetableDetails_Subjects_SubjectId1",
                table: "TimetableDetails",
                column: "SubjectId1",
                principalTable: "Subjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TimetableDetails_Teachers_TeacherId1",
                table: "TimetableDetails",
                column: "TeacherId1",
                principalTable: "Teachers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TimetableDetails_Timetables_TimetableId1",
                table: "TimetableDetails",
                column: "TimetableId1",
                principalTable: "Timetables",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Branches_BranchId",
                table: "Users",
                column: "BranchId",
                principalTable: "Branches",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Applications_Teachers_TeacherId",
                table: "Applications");

            migrationBuilder.DropForeignKey(
                name: "FK_DailyDiaries_Students_StudentId",
                table: "DailyDiaries");

            migrationBuilder.DropForeignKey(
                name: "FK_DailyDiaryDetail_Subjects_SubjectId",
                table: "DailyDiaryDetail");

            migrationBuilder.DropForeignKey(
                name: "FK_DatesheetDetails_Subjects_SubjectId1",
                table: "DatesheetDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_Datesheets_Classes_ClassId",
                table: "Datesheets");

            migrationBuilder.DropForeignKey(
                name: "FK_Datesheets_ExamTests_ExamTestId",
                table: "Datesheets");

            migrationBuilder.DropForeignKey(
                name: "FK_EmpAttendances_Employees_EmployeeId",
                table: "EmpAttendances");

            migrationBuilder.DropForeignKey(
                name: "FK_Employees_Departments_DepartmentId",
                table: "Employees");

            migrationBuilder.DropForeignKey(
                name: "FK_Employees_Designations_DesignationId",
                table: "Employees");

            migrationBuilder.DropForeignKey(
                name: "FK_Fees_Students_StudentId",
                table: "Fees");

            migrationBuilder.DropForeignKey(
                name: "FK_LessonPlans_Subjects_SubjectId",
                table: "LessonPlans");

            migrationBuilder.DropForeignKey(
                name: "FK_Pays_SalarySlips_SalarySlipId",
                table: "Pays");

            migrationBuilder.DropForeignKey(
                name: "FK_SpecialNotes_Sections_SectionId",
                table: "SpecialNotes");

            migrationBuilder.DropForeignKey(
                name: "FK_SpecialNotes_Students_StudentId",
                table: "SpecialNotes");

            migrationBuilder.DropForeignKey(
                name: "FK_Students_Fees_FeeId",
                table: "Students");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectResult_ExamTests_ExamTestId",
                table: "SubjectResult");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectResult_Sections_SectionId",
                table: "SubjectResult");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectResult_Subjects_SubjectId",
                table: "SubjectResult");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectResult_Teachers_TeacherId",
                table: "SubjectResult");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectResultDetails_ExamTests_ExamTestId",
                table: "SubjectResultDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectResultDetails_SubjectResult_SubjectResultId",
                table: "SubjectResultDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_Syllabi_Classes_ClassId",
                table: "Syllabi");

            migrationBuilder.DropForeignKey(
                name: "FK_Syllabi_ExamTests_ExamTestId",
                table: "Syllabi");

            migrationBuilder.DropForeignKey(
                name: "FK_SyllabusDetails_Subjects_SubjectId",
                table: "SyllabusDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_Teachers_Sections_SectionId",
                table: "Teachers");

            migrationBuilder.DropForeignKey(
                name: "FK_TimetableDetails_Subjects_SubjectId1",
                table: "TimetableDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_TimetableDetails_Teachers_TeacherId1",
                table: "TimetableDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_TimetableDetails_Timetables_TimetableId1",
                table: "TimetableDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Branches_BranchId",
                table: "Users");

            migrationBuilder.DropTable(
                name: "ClassExamTest");

            migrationBuilder.DropTable(
                name: "ClassSubject");

            migrationBuilder.DropTable(
                name: "SectionStudent");

            migrationBuilder.DropTable(
                name: "SectionTeacher");

            migrationBuilder.DropTable(
                name: "StudentSubject");

            migrationBuilder.DropTable(
                name: "SubjectTeacher");

            migrationBuilder.DropIndex(
                name: "IX_TimetableDetails_SubjectId1",
                table: "TimetableDetails");

            migrationBuilder.DropIndex(
                name: "IX_TimetableDetails_TeacherId1",
                table: "TimetableDetails");

            migrationBuilder.DropIndex(
                name: "IX_TimetableDetails_TimetableId1",
                table: "TimetableDetails");

            migrationBuilder.DropIndex(
                name: "IX_Teachers_SectionId",
                table: "Teachers");

            migrationBuilder.DropIndex(
                name: "IX_Syllabi_ClassId",
                table: "Syllabi");

            migrationBuilder.DropIndex(
                name: "IX_Syllabi_ExamTestId",
                table: "Syllabi");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SubjectResult",
                table: "SubjectResult");

            migrationBuilder.DropIndex(
                name: "IX_SpecialNotes_SectionId",
                table: "SpecialNotes");

            migrationBuilder.DropIndex(
                name: "IX_Fees_StudentId",
                table: "Fees");

            migrationBuilder.DropIndex(
                name: "IX_Datesheets_ClassId",
                table: "Datesheets");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UserType",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Timetables");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "TimetableDetails");

            migrationBuilder.DropColumn(
                name: "SubjectId",
                table: "TimetableDetails");

            migrationBuilder.DropColumn(
                name: "SubjectId1",
                table: "TimetableDetails");

            migrationBuilder.DropColumn(
                name: "TeacherId1",
                table: "TimetableDetails");

            migrationBuilder.DropColumn(
                name: "TimetableId1",
                table: "TimetableDetails");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Teachers");

            migrationBuilder.DropColumn(
                name: "SectionId",
                table: "Teachers");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "SyllabusDetails");

            migrationBuilder.DropColumn(
                name: "ClassId",
                table: "Syllabi");

            migrationBuilder.DropColumn(
                name: "ExamTestId",
                table: "Syllabi");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Syllabi");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "SubjectResultDetails");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "SubjectResult");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Students");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "StudentGatePasses");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "StuAttendances");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "StuAttendanceDetails");

            migrationBuilder.DropColumn(
                name: "SectionId",
                table: "SpecialNotes");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "SectionIncharges");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "Schools");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                table: "Schools");

            migrationBuilder.DropColumn(
                name: "PhoneNo",
                table: "Schools");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "Schools");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "SalarySlips");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Pays");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "PayrollCategories");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Parents");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "NotificationDetails");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "NewsUpdates");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "LessonPlans");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "LessonPlanDetails");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Fees");

            migrationBuilder.DropColumn(
                name: "StudentId",
                table: "Fees");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "FeeCategories");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "ExamTests");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "Department",
                table: "Employees");

            migrationBuilder.DropColumn(
                name: "Designation",
                table: "Employees");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Employees");

            migrationBuilder.DropColumn(
                name: "EmpId",
                table: "EmpAttendances");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "EmpAttendances");

            migrationBuilder.DropColumn(
                name: "ClassId",
                table: "Datesheets");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "DatesheetDetails");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "DailyDiaryDetail");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "DailyDiaries");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Complaints");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "Branches");

            migrationBuilder.DropColumn(
                name: "DeletedAt",
                table: "Branches");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Branches");

            migrationBuilder.DropColumn(
                name: "TotalStudents",
                table: "Branches");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "Branches");

            migrationBuilder.RenameTable(
                name: "SubjectResult",
                newName: "SubjectResults");

            migrationBuilder.RenameColumn(
                name: "RecentInstitue",
                table: "Teachers",
                newName: "RecentInstitute");

            migrationBuilder.RenameColumn(
                name: "SubjectId",
                table: "SyllabusDetails",
                newName: "SubjectClassRId");

            migrationBuilder.RenameIndex(
                name: "IX_SyllabusDetails_SubjectId",
                table: "SyllabusDetails",
                newName: "IX_SyllabusDetails_SubjectClassRId");

            migrationBuilder.RenameColumn(
                name: "ExamTestId",
                table: "SubjectResultDetails",
                newName: "ClassExamTestRId");

            migrationBuilder.RenameIndex(
                name: "IX_SubjectResultDetails_ExamTestId",
                table: "SubjectResultDetails",
                newName: "IX_SubjectResultDetails_ClassExamTestRId");

            migrationBuilder.RenameColumn(
                name: "SubjectId",
                table: "SubjectResults",
                newName: "SubjectClassRId");

            migrationBuilder.RenameColumn(
                name: "ExamTestId",
                table: "SubjectResults",
                newName: "ClassExamTestRId");

            migrationBuilder.RenameIndex(
                name: "IX_SubjectResult_TeacherId",
                table: "SubjectResults",
                newName: "IX_SubjectResults_TeacherId");

            migrationBuilder.RenameIndex(
                name: "IX_SubjectResult_SubjectId",
                table: "SubjectResults",
                newName: "IX_SubjectResults_SubjectClassRId");

            migrationBuilder.RenameIndex(
                name: "IX_SubjectResult_SectionId",
                table: "SubjectResults",
                newName: "IX_SubjectResults_SectionId");

            migrationBuilder.RenameIndex(
                name: "IX_SubjectResult_ExamTestId",
                table: "SubjectResults",
                newName: "IX_SubjectResults_ClassExamTestRId");

            migrationBuilder.RenameColumn(
                name: "StudentId",
                table: "SpecialNotes",
                newName: "StudentSectionRId");

            migrationBuilder.RenameIndex(
                name: "IX_SpecialNotes_StudentId",
                table: "SpecialNotes",
                newName: "IX_SpecialNotes_StudentSectionRId");

            migrationBuilder.RenameColumn(
                name: "SubjectId",
                table: "LessonPlans",
                newName: "SubjectClassRId");

            migrationBuilder.RenameIndex(
                name: "IX_LessonPlans_SubjectId",
                table: "LessonPlans",
                newName: "IX_LessonPlans_SubjectClassRId");

            migrationBuilder.RenameColumn(
                name: "ExamTestId",
                table: "Datesheets",
                newName: "ClassExamTestRId");

            migrationBuilder.RenameIndex(
                name: "IX_Datesheets_ExamTestId",
                table: "Datesheets",
                newName: "IX_Datesheets_ClassExamTestRId");

            migrationBuilder.RenameColumn(
                name: "SubjectId1",
                table: "DatesheetDetails",
                newName: "SubjectClassRId1");

            migrationBuilder.RenameColumn(
                name: "SubjectId",
                table: "DatesheetDetails",
                newName: "SubjectClassRId");

            migrationBuilder.RenameIndex(
                name: "IX_DatesheetDetails_SubjectId1",
                table: "DatesheetDetails",
                newName: "IX_DatesheetDetails_SubjectClassRId1");

            migrationBuilder.RenameColumn(
                name: "SubjectId",
                table: "DailyDiaryDetail",
                newName: "SubjectClassRId");

            migrationBuilder.RenameIndex(
                name: "IX_DailyDiaryDetail_SubjectId",
                table: "DailyDiaryDetail",
                newName: "IX_DailyDiaryDetail_SubjectClassRId");

            migrationBuilder.RenameColumn(
                name: "StudentId",
                table: "DailyDiaries",
                newName: "TeacherId");

            migrationBuilder.RenameIndex(
                name: "IX_DailyDiaries_StudentId",
                table: "DailyDiaries",
                newName: "IX_DailyDiaries_TeacherId");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Users",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<string>(
                name: "Gender",
                table: "Users",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "BranchId",
                table: "Users",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Religion",
                table: "Users",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TimetableId",
                table: "TimetableDetails",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TeacherId",
                table: "TimetableDetails",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "StartTime",
                table: "TimetableDetails",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<DateTime>(
                name: "EndTime",
                table: "TimetableDetails",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<int>(
                name: "SubjectClassRId",
                table: "TimetableDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Subjects",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<int>(
                name: "FeeId",
                table: "Students",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SectionId",
                table: "Students",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "Status",
                table: "Students",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Sections",
                maxLength: 10,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AddColumn<string>(
                name: "PhoneNum",
                table: "Schools",
                maxLength: 11,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<int>(
                name: "SalarySlipId",
                table: "Pays",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DesignationId",
                table: "Employees",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DepartmentId",
                table: "Employees",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Status",
                table: "Employees",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<int>(
                name: "EmployeeId",
                table: "EmpAttendances",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Departments",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Datesheets",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Classes",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Branches",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50);

            migrationBuilder.AlterColumn<int>(
                name: "TeacherId",
                table: "Applications",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "EmployeeId",
                table: "Applications",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "Status",
                table: "AcademicYears",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_SubjectResults",
                table: "SubjectResults",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "ClassExamTestR",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClassId = table.Column<int>(nullable: false),
                    ExamTestId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassExamTestR", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClassExamTestR_Classes_ClassId",
                        column: x => x.ClassId,
                        principalTable: "Classes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClassExamTestR_ExamTests_ExamTestId",
                        column: x => x.ExamTestId,
                        principalTable: "ExamTests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StudentSectionRs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SectionId = table.Column<int>(nullable: false),
                    StudentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentSectionRs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StudentSectionRs_Sections_SectionId",
                        column: x => x.SectionId,
                        principalTable: "Sections",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentSectionRs_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubjectClassRs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClassId = table.Column<int>(nullable: false),
                    SubjectId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubjectClassRs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubjectClassRs_Classes_ClassId",
                        column: x => x.ClassId,
                        principalTable: "Classes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubjectClassRs_Subjects_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubjectSectionTeacherRs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SectionId = table.Column<int>(nullable: false),
                    SubjectId = table.Column<int>(nullable: false),
                    TeacherId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubjectSectionTeacherRs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubjectSectionTeacherRs_Sections_SectionId",
                        column: x => x.SectionId,
                        principalTable: "Sections",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubjectSectionTeacherRs_Subjects_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubjectSectionTeacherRs_Teachers_TeacherId",
                        column: x => x.TeacherId,
                        principalTable: "Teachers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TimetableDetails_SubjectClassRId",
                table: "TimetableDetails",
                column: "SubjectClassRId");

            migrationBuilder.CreateIndex(
                name: "IX_TimetableDetails_TeacherId",
                table: "TimetableDetails",
                column: "TeacherId");

            migrationBuilder.CreateIndex(
                name: "IX_TimetableDetails_TimetableId",
                table: "TimetableDetails",
                column: "TimetableId");

            migrationBuilder.CreateIndex(
                name: "IX_Syllabi_ClassExamTestRId",
                table: "Syllabi",
                column: "ClassExamTestRId");

            migrationBuilder.CreateIndex(
                name: "IX_Students_SectionId",
                table: "Students",
                column: "SectionId");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_UserId",
                table: "Employees",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Applications_EmployeeId",
                table: "Applications",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassExamTestR_ClassId",
                table: "ClassExamTestR",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_ClassExamTestR_ExamTestId",
                table: "ClassExamTestR",
                column: "ExamTestId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentSectionRs_SectionId",
                table: "StudentSectionRs",
                column: "SectionId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentSectionRs_StudentId",
                table: "StudentSectionRs",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectClassRs_ClassId",
                table: "SubjectClassRs",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectClassRs_SubjectId",
                table: "SubjectClassRs",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectSectionTeacherRs_SectionId",
                table: "SubjectSectionTeacherRs",
                column: "SectionId");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectSectionTeacherRs_SubjectId",
                table: "SubjectSectionTeacherRs",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectSectionTeacherRs_TeacherId",
                table: "SubjectSectionTeacherRs",
                column: "TeacherId");

            migrationBuilder.AddForeignKey(
                name: "FK_Applications_Employees_EmployeeId",
                table: "Applications",
                column: "EmployeeId",
                principalTable: "Employees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Applications_Teachers_TeacherId",
                table: "Applications",
                column: "TeacherId",
                principalTable: "Teachers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DailyDiaries_Teachers_TeacherId",
                table: "DailyDiaries",
                column: "TeacherId",
                principalTable: "Teachers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DailyDiaryDetail_SubjectClassRs_SubjectClassRId",
                table: "DailyDiaryDetail",
                column: "SubjectClassRId",
                principalTable: "SubjectClassRs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DatesheetDetails_SubjectClassRs_SubjectClassRId1",
                table: "DatesheetDetails",
                column: "SubjectClassRId1",
                principalTable: "SubjectClassRs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Datesheets_ClassExamTestR_ClassExamTestRId",
                table: "Datesheets",
                column: "ClassExamTestRId",
                principalTable: "ClassExamTestR",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EmpAttendances_Employees_EmployeeId",
                table: "EmpAttendances",
                column: "EmployeeId",
                principalTable: "Employees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_Departments_DepartmentId",
                table: "Employees",
                column: "DepartmentId",
                principalTable: "Departments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_Designations_DesignationId",
                table: "Employees",
                column: "DesignationId",
                principalTable: "Designations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Employees_Users_UserId",
                table: "Employees",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_LessonPlans_SubjectClassRs_SubjectClassRId",
                table: "LessonPlans",
                column: "SubjectClassRId",
                principalTable: "SubjectClassRs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Pays_SalarySlips_SalarySlipId",
                table: "Pays",
                column: "SalarySlipId",
                principalTable: "SalarySlips",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SpecialNotes_StudentSectionRs_StudentSectionRId",
                table: "SpecialNotes",
                column: "StudentSectionRId",
                principalTable: "StudentSectionRs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Students_Fees_FeeId",
                table: "Students",
                column: "FeeId",
                principalTable: "Fees",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Students_Sections_SectionId",
                table: "Students",
                column: "SectionId",
                principalTable: "Sections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectResultDetails_ClassExamTestR_ClassExamTestRId",
                table: "SubjectResultDetails",
                column: "ClassExamTestRId",
                principalTable: "ClassExamTestR",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectResultDetails_SubjectResults_SubjectResultId",
                table: "SubjectResultDetails",
                column: "SubjectResultId",
                principalTable: "SubjectResults",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectResults_ClassExamTestR_ClassExamTestRId",
                table: "SubjectResults",
                column: "ClassExamTestRId",
                principalTable: "ClassExamTestR",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectResults_Sections_SectionId",
                table: "SubjectResults",
                column: "SectionId",
                principalTable: "Sections",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectResults_SubjectClassRs_SubjectClassRId",
                table: "SubjectResults",
                column: "SubjectClassRId",
                principalTable: "SubjectClassRs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectResults_Teachers_TeacherId",
                table: "SubjectResults",
                column: "TeacherId",
                principalTable: "Teachers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Syllabi_ClassExamTestR_ClassExamTestRId",
                table: "Syllabi",
                column: "ClassExamTestRId",
                principalTable: "ClassExamTestR",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SyllabusDetails_SubjectClassRs_SubjectClassRId",
                table: "SyllabusDetails",
                column: "SubjectClassRId",
                principalTable: "SubjectClassRs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TimetableDetails_SubjectClassRs_SubjectClassRId",
                table: "TimetableDetails",
                column: "SubjectClassRId",
                principalTable: "SubjectClassRs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TimetableDetails_Teachers_TeacherId",
                table: "TimetableDetails",
                column: "TeacherId",
                principalTable: "Teachers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TimetableDetails_Timetables_TimetableId",
                table: "TimetableDetails",
                column: "TimetableId",
                principalTable: "Timetables",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Branches_BranchId",
                table: "Users",
                column: "BranchId",
                principalTable: "Branches",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
