﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using EdifyPracticeFinal.Models;
using EdifyPracticeFinal.Models;

namespace EdifyPracticeFinal.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<AcademicYear> AcademicYears { get; set; }
        public DbSet<Application> Applications { get; set; }
        public DbSet<Branch> Branches { get; set; }
        public DbSet<Class> Classes { get; set; }
        public DbSet<Complaint> Complaints { get; set; }
        public DbSet<Datesheet> Datesheets { get; set; }
        public DbSet<DatesheetDetail> DatesheetDetails { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Designation> Designations { get; set; }
        public DbSet<EmpAttendance> EmpAttendances { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<ExamTest> ExamTests { get; set; }
        public DbSet<Fee> Fees { get; set; }
        public DbSet<FeeCategory> FeeCategories { get; set; }
        public DbSet<LessonPlan> LessonPlans { get; set; }
        public DbSet<LessonPlanDetail> LessonPlanDetails { get; set; }
        public DbSet<NewsUpdate> NewsUpdates { get; set; }
        public DbSet<NotificationDetail> NotificationDetails { get; set; }
        public DbSet<Parent> Parents { get; set; }
        public DbSet<Pay> Pays { get; set; }
        public DbSet<PayrollCategory> PayrollCategories { get; set; }
        public DbSet<SalarySlip> SalarySlips { get; set; }
        public DbSet<School> Schools { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<SectionIncharge> SectionIncharges { get; set; }
        public DbSet<StuAttendance> StuAttendances { get; set; }
        public DbSet<StuAttendanceDetail> StuAttendanceDetails { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<StudentGatePass> StudentGatePasses { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<SubjectResultDetail> SubjectResultDetails { get; set; }
        public DbSet<Syllabus> Syllabi { get; set; }
        public DbSet<SyllabusDetail> SyllabusDetails { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Timetable> Timetables { get; set; }
        public DbSet<TimetableDetail> TimetableDetails { get; set; }
        public DbSet<DailyDiary> DailyDiaries { get; set; }
        public DbSet<SpecialNote> SpecialNotes { get; set; }

    }
}
