﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace EdifyPracticeFinal.Controllers
{
    public class BackendController : Controller
    {
        public IActionResult SchoolWizardForm()
        {
            return View("~/Views/Backend/SuperAdmin/SchoolWizardForm.cshtml");
        }

        public IActionResult SchoolsList()
        {
            return View("~/Views/Backend/SuperAdmin/SchoolsList.cshtml");
        }

        public IActionResult CreateNewBranch()
        {
            return View("~/Views/Backend/SuperAdmin/CreateNewBranch.cshtml");
        }

        public IActionResult CreateSchoolAdmin()
        {
            return View("~/Views/Backend/SuperAdmin/CreateSchoolAdmin.cshtml");
        }
}
}