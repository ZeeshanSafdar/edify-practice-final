﻿namespace EdifyPracticeFinal.Common.Enums
{
    public enum AttendanceStatus
    {
        Present = 1,
        Absent,
        Leave
    }
}