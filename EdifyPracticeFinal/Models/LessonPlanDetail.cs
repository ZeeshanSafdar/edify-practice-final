﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPracticeFinal.Models
{
    public class LessonPlanDetail : BaseModel
    {

        [Required]
        [StringLength(10)]
        public string Day { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required]
        [StringLength(100)]
        public string SyllabusDetail { get; set; }


        #region BELONGS TO FIELDS


        public int LessonPlanId { get; set; }

        public virtual LessonPlan LessonPlan { get; set; }

        #endregion


    }
}
