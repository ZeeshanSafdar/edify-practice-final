﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPracticeFinal.Models
{
    public class Application
    {
        public int Id { get; set; }

        [Required]
        public string ApplicationSubject { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime ApplicationDate { get; set; }



        #region BELONGS TO FIELDS

        public int ParentId { get; set; }
        public int TeacherId { get; set; }

        public virtual Parent Parent { get; set; }
        public virtual Teacher Teacher { get; set; }

        #endregion



    }
}
