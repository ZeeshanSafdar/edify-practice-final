﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPracticeFinal.Models
{
    public class Section : BaseModel
    {

        [Required]
        public int TotalStudents { get; set; }


        #region BELONGS TO FIELDS
        
        public int ClassId { get; set; }
        public virtual Class Class { get; set; }

        #endregion


        #region HAS MANY FIELDS
        
        public virtual Collection<SectionIncharge> SectionIncharges { get; set; }
        public virtual Collection<SectionStudent> SectionStudents { get; set; }
        public virtual Collection<SectionTeacher> SectionTeachers { get; set; }
        //        public virtual Collection<Student> Students { get; set; }
        public virtual Collection<DailyDiary> DailyDiaries { get; set; }
        public virtual Collection<Teacher> Teachers { get; set; }
        public virtual Collection<LessonPlan> LessonPlans { get; set; }
        public virtual Collection<SubjectResult> SubjectResults { get; set; }

        #endregion


    }
}
