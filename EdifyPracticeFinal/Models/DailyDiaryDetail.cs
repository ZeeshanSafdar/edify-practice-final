﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPracticeFinal.Models
{
    public class DailyDiaryDetail : BaseModel
    {

        [Required]
        public string Detail { get; set; }

        public string Note { get; set; }

        #region BELONGS TO FIELDS
        
        public int SubjectId { get; set; }
        public int TeacherId { get; set; }
        public int DailyDiaryId { get; set; }
        
        public virtual Subject Subject { get; set; }
        public virtual Teacher Teacher { get; set; }
        public virtual DailyDiary DailyDiary { get; set; }

        #endregion



    }
}
