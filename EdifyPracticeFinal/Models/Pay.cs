﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPracticeFinal.Models
{
    public class Pay : BaseModel
    {
        [Required]
        public double Amount { get; set; }


        #region HAS MANY FIELDS

        public virtual Collection<PayrollCategory> PayrollCategories { get; set; }

        #endregion
    }
}
