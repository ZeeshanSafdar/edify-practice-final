﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPracticeFinal.Models
{
    public class SyllabusDetail : BaseModel
    {

        [Required]
        public string Detail { get; set; }



        #region BELONGS TO FIELDS

        public int SubjectId { get; set; }

        public int SyllabusId { get; set; }

        public virtual Subject Subject { get; set; }
        public virtual Syllabus Syllabus { get; set; }

        #endregion



    }
}
