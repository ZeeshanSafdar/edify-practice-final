﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPracticeFinal.Models
{
    public class Department : BaseModel
    {

        public int TotalEmployee { get; set; }

        #region BELONGS TO FIELDS

        public int SchoolId { get; set; }
        public virtual School School { get; set; }

        #endregion


        #region HAS MANY FIELDS

        public virtual Collection<Employee> Employees { get; set; }

        #endregion

    }
}
