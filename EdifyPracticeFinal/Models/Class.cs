﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPracticeFinal.Models
{
    public class Class : BaseModel
    {

        [Required]
        public int TotalSections { get; set; }



        #region BELONGS TO FIELDS

        [Required]
        public int BranchId { get; set; }
        public virtual Branch Branch { get; set; }

        
        //        public int SubjectId { get; set; }
        #endregion



        #region HAS MANY FIELDS

        //        public virtual Collection<Subject> Subjects { get; set; }
        //        public virtual Collection<ExamTest> ExamTests { get; set; }
        public virtual Collection<ClassExamTest> ClassExamTests { get; set; }
        public virtual Collection<ClassSubject> ClassSubjects { get; set; }
        public virtual Collection<Section> Sections { get; set; }
        public virtual Collection<Fee> Fees { get; set; }

        #endregion



    }
}
