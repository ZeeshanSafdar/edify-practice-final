﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPracticeFinal.Models
{
    public class SectionStudent
    {
        public int Id { get; set; }

        public int SectionId { get; set; }
        public Section Section { get; set; }

        public int StudentId { get; set; }
        public Student Student { get; set; }
    }
}
