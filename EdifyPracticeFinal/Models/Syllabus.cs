﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPracticeFinal.Models
{
    public class Syllabus : BaseModel
    {

        #region BELONGS TO FIELDS

        public int ClassExamTestRId { get; set; }

        public int ClassId { get; set; }

        public virtual Class Class { get; set; }

//        public virtual ClassExamTestR ClassExamTestR { get; set; }

        #endregion



        #region HAS MANY FIELDS

        public virtual Collection<SyllabusDetail> SyllabusDetails { get; set; }

        #endregion

    }
}
