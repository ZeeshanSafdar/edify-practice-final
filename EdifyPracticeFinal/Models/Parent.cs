﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPracticeFinal.Models
{
    public class Parent : BaseModel 
    {

        public string Profession { get; set; }

        public string MonthlyIncome { get; set; }


        #region BELONGS TO FIELDS

        public int UserId { get; set; }

        #endregion



        #region HAS MANY FIELDS

        public virtual Collection<Student> Students { get; set; }
        public virtual Collection<Application> Applications { get; set; }
        public virtual Collection<Complaint> Complaints { get; set; }

        #endregion

    }
}
