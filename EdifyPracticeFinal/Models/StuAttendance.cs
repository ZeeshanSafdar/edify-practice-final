﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPracticeFinal.Models
{
    public class StuAttendance : BaseModel
    {

        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }


        #region BELONGS TO FIELDS

        public int SectionId { get; set; }

        public virtual Section Section { get; set; }

        #endregion

        #region HAS MANY FIELDS

        public virtual Collection<StuAttendanceDetail> StuAttendanceDetails { get; set; }

        #endregion

    }
}
