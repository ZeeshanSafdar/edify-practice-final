﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPracticeFinal.Models
{
    public class StuAttendanceDetail : BaseModel
    {

        [Required]
        [StringLength(10)]
        public string Status { get; set; }


        #region BELONGS TO FIELDS

        public int StudentId { get; set; }

        public int StuAttendanceId { get; set; }

        public virtual Student Student { get; set; }

        public virtual StuAttendance StuAttendance { get; set; }

        #endregion



    }
}
