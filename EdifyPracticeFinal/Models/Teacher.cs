﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace EdifyPracticeFinal.Models
{
    public class Teacher : BaseModel
    {
  
        public string Expertise { get; set; }
        public string TeachingHistory { get; set; }
        public string RecentInstitue { get; set; }


        #region BELONGS TO FIELDS

        public int UserId { get; set; }

        #endregion



        #region HAS MANY FIELDS

        //        public virtual Collection<Subject> Subjects { get; set; }
        public virtual Collection<SubjectTeacher> SubjectTeachers { get; set; }
        public virtual Collection<SectionTeacher> SectionTeachers { get; set; }
//        public virtual Collection<Section> Sections  { get; set; }
        public virtual Collection<Application> Applications { get; set; }
        public virtual Collection<SubjectResult> SubjectResults { get; set; }
        public virtual Collection<LessonPlan> LessonPlans { get; set; }
        public virtual Collection<DailyDiaryDetail> DailyDiaryDetails { get; set; }
        public virtual Collection<TimetableDetail> TimetableDetails { get; set; }

        #endregion



    }
}
