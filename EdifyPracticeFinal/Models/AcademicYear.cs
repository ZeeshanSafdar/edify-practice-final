﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace EdifyPracticeFinal.Models
{
    public class AcademicYear : BaseModel
    {
        [Required]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }
        

        #region BELONGS TO FIELDS

        public int SchoolId { get; set; }
        public virtual School School { get; set; }

        #endregion

    }
}
