﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using EdifyPracticeFinal.Common.Enums;

namespace EdifyPracticeFinal.Models
{
    public class User : DefaultModel
    {
        public UserType UserType { get; set; }

        [Required]
        [StringLength(50)]
        public string Address { get; set; }

        [Required]
        public Gender Gender { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime DOb { get; set; }

        [Required]
        public int Cnic { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Phone]
        public string PhoneNo { get; set; }

    }
}
