﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPracticeFinal.Models
{
    public class Employee : BaseModel
    {

        [Required]
        public string Designation { get; set; }
        [Required]
        public double CurrentSalary { get; set; }
        [Required]
        public double JoiningSalary { get; set; }
        [Required]
        public DateTime JoiningDate { get; set; }
        [Required]
        public string Qualification { get; set; }
        [Required]
        public int Experience { get; set; }
        [Required]
        public string Department { get; set; }


        #region BELONGS TO FIELDS

        public int UserId { get; set; }

        #endregion



        #region HAS MANY FIELDS

        public virtual Collection<SalarySlip> SalarySlips { get; set; }
        public virtual Collection<EmpAttendance> EmpAttendances { get; set; }
        public virtual Collection<Complaint> Complaints { get; set; }

        #endregion



    }
}
