﻿namespace EdifyPracticeFinal.Models
{
    public class SectionTeacher
    {
        public int Id { get; set; }

        public int TeacherId { get; set; }
        public Teacher Teacher { get; set; }

        public int SectionId { get; set; }
        public Section Section { get; set; }
    }
}