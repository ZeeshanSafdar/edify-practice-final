﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPracticeFinal.Models
{
    public class Timetable : BaseModel
    {

        [Required]
        [StringLength(10)]
        public string Day { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime CreationDate { get; set; }

        [Required]
        public bool Status { get; set; }


        #region BELONGS TO FIELDS

        public int SectionId { get; set; }

        public virtual Section Section { get; set; }

        #endregion


        #region HAS MANY FIELDS

        public virtual Collection<TimetableDetail> TimetableDetails { get; set; }

        #endregion

    }
}
