﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EdifyPracticeFinal.Models
{
    public class DatesheetDetail : BaseModel
    {   
        [Required]
        public string Day { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Date { get; set; }

        public DateTime Time { get; set; }


        #region BELONGS TO FIELDS

        public string SubjectId { get; set; }
        public int DatesheetId { get; set; }

        public virtual Datesheet Datesheet { get; set; }
        public virtual Subject Subject { get; set; }

        #endregion


    }
}
