﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using EdifyPracticeFinal.Models;

namespace EdifyPracticeFinal.Models
{
    public class Fee : BaseModel
    {
       
        [Required]
        public double Amount { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [Required]
        public string Status { get; set; }


        #region BELONGS TO FIELDS

        public int StudentId { get; set; }

        public int ClassId { get; set; }

        public virtual Student Student { get; set; }
        public virtual Class Class { get; set; }

        #endregion



        #region HAS MANY FIELDS

        public virtual Collection<Student> Students { get; set; }

        #endregion


    }
}
